# OpenML dataset: Technical-Indicator-Backtest

https://www.openml.org/d/43736

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Using TA-Lib : Technical Analysis Library. Backtest on the SPY Index data, using support and resistance indicators or any other indicator. 
Content
Data contains daily SPY Index: 
Date    Open    High    Low Close   Adj Close   Volume
Acknowledgements
Support for Resistance: Technical Analysis
WIKI link: https://en.wikipedia.org/wiki/Support_and_resistance
Inspiration
Do your best for the backtest and technical indicator implementation

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43736) of an [OpenML dataset](https://www.openml.org/d/43736). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43736/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43736/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43736/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

